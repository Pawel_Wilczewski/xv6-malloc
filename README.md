# xv6-malloc

`malloc()` and `free()` for xv6 systems. Created for one of the university assignments. It's space efficient, correctly aligns memory for any variable type and utilizes paging for better performance.
