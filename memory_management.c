#include "memory_management.h"
#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"
#include "kernel/param.h"

#define NULL ((void *) 0)
#define PAGE_SIZE (4096)

static MemHeader start;
static MemHeader *freeMem = NULL;
static MemHeader *usedMem = NULL; // used for freeing pages, not in order unlike freeMem

static int canReleaseMemory = 1;

static void add_used_block(MemHeader *block)
{
	if (usedMem == NULL)
	{
		usedMem = block;
		block->m.next = usedMem;
	}
	else
	{
		MemHeader *left = usedMem;
		MemHeader *right = usedMem->m.next;
		left->m.next = block;
		block->m.next = right;
	}
}

static void remove_used_block(MemHeader *block)
{
	if (usedMem == NULL || block == NULL) return;
	
	MemHeader *previous = usedMem;
	MemHeader *current = usedMem->m.next;
	do
	{
		if (current == previous && current == block)
		{
			usedMem = NULL;
			return;
		}
		if (current == block)
		{
			previous->m.next = block->m.next;
			
			if (block == usedMem)
				usedMem = previous;
			
			return;
		}
		
		previous = previous->m.next;
		current = current->m.next;
	} while (previous != usedMem);
}

static MemHeader *get_highest_used_block()
{
	if (usedMem == NULL)
		return NULL;
		
	MemHeader *max = NULL;
	MemHeader *current = usedMem;
	do
	{
		if (current > max)
			max = current;
		
		current = current->m.next;
	} while (current != usedMem);
	
	return max;
}

static MemHeader *get_highest_free_block()
{
	if (freeMem == NULL)
		return NULL;
		
	MemHeader *max = NULL;
	MemHeader *current = freeMem;
	do
	{
		if (current > max)
			max = current;
		
		current = current->m.next;
	} while (current != freeMem);
	
	return max;
}

static void try_merge_all_blocks() // inefficient and easily avoidable, but 100% reliable
{
	int merged = 0;
	MemHeader *previousBlock = freeMem;
	MemHeader *currentBlock = freeMem->m.next;
	do
	{
		if (previousBlock < currentBlock && previousBlock + previousBlock->m.size == currentBlock)
		{
			MemHeader *newBlock = previousBlock;
			newBlock->m.size = previousBlock->m.size + currentBlock->m.size;
			newBlock->m.next = currentBlock->m.next;
			// if we have merged blocks, the current and previous blocks are irrelevant
			// therefore, break and do the whole thing over (recursion genius, 300 iq)
			merged = 1;
			break;
		}
		
		previousBlock = previousBlock->m.next;
		currentBlock = currentBlock->m.next;
	} while (previousBlock != freeMem);
	
	if (merged)
		try_merge_all_blocks();
}

void *_malloc(int size)
{
	if (size <= 0)
		return NULL;
	
	// calculate the number of units required	
	// ceil(size/size of one unit) + 1 for the initial header
	int numUnits = size / sizeof(MemHeader) + (size % sizeof(MemHeader) != 0) + 1;
	
	// init freeMem if it hasn't been done yet
	if (freeMem == NULL)
	{
		freeMem = &start;
		
		start.m.next = freeMem;
		start.m.size = 0; // by making the size of this block 0, it will never be merged with other blocks and this is DESIRED
	}
	
	
	MemHeader *previousFreeBlock = freeMem;
	MemHeader *freeBlock = freeMem->m.next;
	do
	{
		// exactly sized block
		if (freeBlock->m.size == numUnits)
		{
			previousFreeBlock->m.next = freeBlock->m.next;
			
			add_used_block(freeBlock);
			return (void *) (freeBlock + 1);
		}
		// bigger than required block
		else if (freeBlock->m.size > numUnits)
		{
			// splits the blocks into two, the first will get the size of numUnits, and returns the pointer to the first block's data
			// also the blocks are appropriately added to the linked list
			MemHeader *block1 = freeBlock;
			MemHeader *block2 = freeBlock + numUnits;
			block2->m.size = freeBlock->m.size - numUnits;
			block1->m.size = numUnits;
			
			block2->m.next = freeBlock->m.next;
			block1->m.next = NULL;
			
			previousFreeBlock->m.next = block2;
			
			add_used_block(block1);
			
			return (void *) (block1 + 1);
			
		}
		
		freeBlock = freeBlock->m.next;
		previousFreeBlock = previousFreeBlock->m.next;
	} while (freeBlock != freeMem->m.next);
	
	// if we haven't found a memory block of required size, we need to sbrk
	
	// request more memory for the program
	// we want to allocate at least PAGE_SIZE
	// for better performance, as sbrk is costly
	int unitsToAllocate = numUnits;
	if (numUnits < PAGE_SIZE)
		unitsToAllocate = PAGE_SIZE;
	
	// allocate the required amount of space
	char *newMem = sbrk(unitsToAllocate * sizeof(MemHeader));
	
	if (newMem == (char *) -1)
		return NULL;
	
	// create the header for the new memory and add it to the linked list
	MemHeader *newHeader = (MemHeader *) newMem;
	newHeader->m.size = unitsToAllocate;
	canReleaseMemory = 0; // we don't want to release memory because we are only putting the block in the right spot
	_free(newHeader + 1); // free is gonna put this block in the right spot
	canReleaseMemory = 1;
	
	// we've added more free memory
	// therefore, the next _malloc will find a free block and return that
	return _malloc(size);
}

void _free(void *ptr)
{
	if (ptr == NULL)
		return;
		
	MemHeader *ptrHeader = (MemHeader *) ptr - 1;
	
	remove_used_block(ptrHeader);
	
	// we're looking for left and right blocks to put the free memory between
	MemHeader *rightBlock = NULL;
	MemHeader *leftBlock = NULL;
	MemHeader *lastBlock = NULL;
	MemHeader *previousBlock = freeMem;
	MemHeader *currentBlock = freeMem->m.next;
	do
	{
		if (previousBlock < ptrHeader && ptrHeader < currentBlock)
		{
			rightBlock = currentBlock;
			leftBlock = previousBlock;
			break;
		}
		
		lastBlock = previousBlock;
		previousBlock = previousBlock->m.next;
		currentBlock = currentBlock->m.next;
	} while (previousBlock != freeMem);
	
	if (leftBlock == NULL || rightBlock == NULL)
	{
		// if left and right blocks are null, we're gonna link to the last block in the list
		lastBlock->m.next = ptrHeader;
		ptrHeader->m.next = previousBlock;
	}
	else
	{		
		// found left and right headers that will link to
		// possibly, merge them together
		leftBlock->m.next = ptrHeader;
		ptrHeader->m.next = rightBlock;
	}
	
	// see if we can merge any blocks together
	try_merge_all_blocks();
	
	// PAGES:
	// keep track of used blocks
	// if the highest address used block is below the highest size of the free blocks
	// and if the last free block is of size > page size
	// release the memory to the system
	MemHeader *maxFree = get_highest_free_block();
	MemHeader *maxUsed = get_highest_used_block();
	if (canReleaseMemory && maxFree != NULL && maxFree > maxUsed && maxFree->m.size > PAGE_SIZE)
	{
		sbrk(PAGE_SIZE - maxFree->m.size);
		maxFree->m.size = PAGE_SIZE;
	}
}

