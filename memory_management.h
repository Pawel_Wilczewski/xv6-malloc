#ifndef MEMORY_MANAGEMENT_H
#define MEMORY_MANAGEMENT_H

union memheader;
typedef union memheader MemHeader;

typedef struct
{
	MemHeader *next;
	int size;
} MemInfo;

union memheader
{
	MemInfo m;
	double _aligner;
};

void *_malloc(int size);
void _free(void *ptr);

#endif // MEMORY_MANAGEMENT_H
